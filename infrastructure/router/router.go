package router

import (
	"github.com/labstack/echo"
	"github.com/rcjuarez/examen_go_rest/interface/controller"
)

func NewRouter(e *echo.Echo, ctrl controller.AppController) *echo.Echo {
	//e.Group("N")
	//Consumir todos
	e.GET("/NutriNET/cliente", func(context echo.Context) error { return ctrl.Cliente.GetClientes(context) })
	//Consumir un cliente
	e.GET("/NutriNET/cliente/:id", func(context echo.Context) error { return ctrl.Cliente.GetCliente(context, context.Param("id")) })
	//Dar de alta un cliente
	e.POST("/NutriNET/cliente", func(context echo.Context) error { return ctrl.Cliente.CreateCliente(context) })
	//Modificar un cliente
	e.PUT("/NutriNET/cliente/:id", func(context echo.Context) error { return ctrl.Cliente.UpdateCliente(context, context.Param("id")) })

	return e
}
