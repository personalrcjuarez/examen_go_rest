package controller

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/rcjuarez/examen_go_rest/domain/model"
	"github.com/rcjuarez/examen_go_rest/usecase/interactor"
)

type Respuesta struct {
	Cve_Error   int
	Cve_Mensaje string
	Object      interface{}
}

const respuestaExitosa = 0
const respuestaNegativa = -1

type Context interface {
	JSON(code int, i interface{}) error
	Bind(i interface{}) error
}

type clienteController struct {
	clienteInteractor interactor.ClienteInteractor
}

type ClienteController interface {
	GetClientes(c Context) error
	GetCliente(c Context, id string) error
	CreateCliente(c Context) error
	UpdateCliente(c Context, id string) error
}

type AppController struct {
	Cliente interface{ ClienteController }
}

func NewClienteController(us interactor.ClienteInteractor) ClienteController {
	return &clienteController{us}
}

//Obtención de todos los clientes
func (cli clienteController) GetClientes(c Context) error {
	data := cli.clienteInteractor.Get()
	if data == nil {
		return c.JSON(http.StatusNotFound, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "No existen clientes",
			Object:      nil,
		})
	}

	return c.JSON(http.StatusNotFound, &Respuesta{
		Cve_Error:   respuestaExitosa,
		Cve_Mensaje: "Clientes encontrados",
		Object:      data,
	})
}

//Obtención del cliente
func (cli clienteController) GetCliente(c Context, id string) error {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Error al obtener el parámetro: " + err.Error(),
			Object:      nil,
		})
	}

	if idInt <= 0 {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Parámetro no válido ",
			Object:      nil,
		})
	}

	cliente := cli.clienteInteractor.GetOne(idInt)
	if cliente == (model.Cliente{}) {
		return c.JSON(http.StatusNotFound, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "No se encontró ningún cliente con el identificador: " + id,
			Object:      nil,
		})
	}

	return c.JSON(http.StatusNotFound, &Respuesta{
		Cve_Error:   respuestaExitosa,
		Cve_Mensaje: "Cliente encontrado",
		Object:      cliente,
	})
}

//Creación del cliente
func (cli clienteController) CreateCliente(c Context) error {
	var cliente model.Cliente
	if err := c.Bind(&cliente); !errors.Is(err, nil) {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Ocurrió un error al tratar la información: " + err.Error(),
			Object:      nil,
		})
	}

	err := cli.clienteInteractor.Create(cliente)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "No se pudo crear la persona: " + err.Error(),
			Object:      nil,
		})
	}

	return c.JSON(http.StatusBadRequest, &Respuesta{
		Cve_Error:   respuestaExitosa,
		Cve_Mensaje: "Persona registrada correctamente",
		Object:      nil,
	})
}

//Actualizacion de cliente
func (cli clienteController) UpdateCliente(c Context, id string) error {
	idInt, err := strconv.Atoi(id)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Error al obtener el parámetro: " + err.Error(),
			Object:      nil,
		})
	}

	if idInt <= 0 {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Parámetro no válido ",
			Object:      nil,
		})
	}

	//mapear la información del cliente
	var cliente model.Cliente
	if err := c.Bind(&cliente); !errors.Is(err, nil) {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Ocurrió un error al tratar la información: " + err.Error(),
			Object:      nil,
		})
	}

	//realizar la actualización
	err = cli.clienteInteractor.Update(cliente, idInt)
	if err != nil {
		return c.JSON(http.StatusBadRequest, &Respuesta{
			Cve_Error:   respuestaNegativa,
			Cve_Mensaje: "Ocurrió un error al tratar la información: " + err.Error(),
			Object:      nil,
		})
	}

	return c.JSON(http.StatusBadRequest, &Respuesta{
		Cve_Error:   respuestaExitosa,
		Cve_Mensaje: "Actualización exitosa: ",
		Object:      nil,
	})
}
