package repository

import (
	"errors"
	"fmt"
	"time"

	"github.com/rcjuarez/examen_go_rest/domain/model"
	"github.com/rcjuarez/examen_go_rest/usecase/interactor"
)

var mapClientes []*model.Cliente
var key int = 1

type newClienteRepo struct{}

func NewClienteRepo() interactor.ClienteRepository {
	return &newClienteRepo{}
}

func (repo newClienteRepo) FindAll() []*model.Cliente { return mapClientes }

func (repo newClienteRepo) FindOne(idCliente int) (cliente model.Cliente) {
	for _, v := range mapClientes {
		fmt.Println(v)
		if v.ClienteID == idCliente {
			return *v
		}
	}
	return
}

func (repo newClienteRepo) CreateOne(cliente model.Cliente) error {
	if existeUsuarioEmail(cliente) {
		return errors.New("No puede insertar valores duplicdos en el nombre de usuario y correo electrónico")
	} else {
		fecha := time.Now()
		cliente.ClienteID = key
		cliente.FechaCreacion = fecha
		cliente.FechaActualizacion = fecha
		mapClientes = append(mapClientes, &cliente)
		key++
		return nil
	}
}

func existeUsuarioEmail(cliente model.Cliente) bool {
	for _, v := range mapClientes {
		if v.NombreUsuario == cliente.NombreUsuario || v.CorreoElectronico == cliente.CorreoElectronico {
			return true
		}
	}
	return false
}

func (repo newClienteRepo) UpdateOne(cliente model.Cliente, idCliente int) error {
	found := false
	for _, x := range mapClientes {
		if x.ClienteID == idCliente {
			if existeUsuarioEmail(cliente) {
				return errors.New("Ya existe un cliente con el mismo nombre de usuario o correo")
			}
			x.Edad = cliente.Edad
			x.Estatura = cliente.Estatura
			x.Peso = cliente.Peso
			x.GEB = cliente.GEB
			found = true
		}
	}

	if !found {
		return errors.New("Cliente no encontrado")
	}
	return nil
}
