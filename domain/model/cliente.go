package model

import "time"

type Cliente struct {
	ClienteID          int       `json:"clienteID"`
	NombreUsuario      string    `json:"Nombre_Usuario"`
	Contrasenia        string    `json:"Contrasenia"`
	Nombre             string    `json:"Nombre"`
	Apellidos          string    `json:"Apellidos"`
	CorreoElectronico  string    `json:"Correo_Electronico"`
	Edad               int       `json:"edad"`
	Estatura           float64   `json:"estatura"`
	Peso               float64   `json:"peso"`
	IMC                float64   `json:"IMC"`
	GEB                float64   `json:"GEB"`
	ETA                float64   `json:"ETA"`
	FechaCreacion      time.Time `json:"fechaCreacion"`
	FechaActualizacion time.Time `json:"fechaActualizacion"`
}
