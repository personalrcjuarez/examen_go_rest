package interactor

import (
	"github.com/rcjuarez/examen_go_rest/domain/model"
)

type ClienteRepository interface {
	FindAll() []*model.Cliente
	FindOne(idCliente int) (cliente model.Cliente)
	CreateOne(cliente model.Cliente) error
	UpdateOne(cliente model.Cliente, idCliente int) error
}

type clienteInteractor struct {
	ClienteRepository ClienteRepository
}

type ClienteInteractor interface {
	Get() []*model.Cliente
	GetOne(idCliente int) (cliente model.Cliente)
	Create(cliente model.Cliente) error
	Update(cliente model.Cliente, idCliente int) error
}

func NewClienteInter(r ClienteRepository) ClienteInteractor {
	return &clienteInteractor{r}
}

func (cli clienteInteractor) Get() []*model.Cliente {
	return cli.ClienteRepository.FindAll()
}

func (cli clienteInteractor) GetOne(idCliente int) (cliente model.Cliente) {
	return cli.ClienteRepository.FindOne(idCliente)
}

func (cli clienteInteractor) Create(cliente model.Cliente) error {
	return cli.ClienteRepository.CreateOne(cliente)
}

func (cli clienteInteractor) Update(cliente model.Cliente, idCliente int) error {
	return cli.ClienteRepository.UpdateOne(cliente, idCliente)
}
