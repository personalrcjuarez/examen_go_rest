package registry

import (
	"github.com/rcjuarez/examen_go_rest/interface/controller"
	ifzrepo "github.com/rcjuarez/examen_go_rest/interface/repository"
	"github.com/rcjuarez/examen_go_rest/usecase/interactor"
)

type registry struct {
}

type Registry interface {
	NewAppController() controller.AppController
}

func NewRegistry() Registry {
	return &registry{}
}

func (r registry) NewClienteRepo() interactor.ClienteRepository {
	return ifzrepo.NewClienteRepo()
}

func (r registry) NewClienteInter() interactor.ClienteInteractor {
	return interactor.NewClienteInter(r.NewClienteRepo())
}

func (r registry) NewClienteControl() controller.ClienteController {
	return controller.NewClienteController(r.NewClienteInter())
}

func (r *registry) NewAppController() controller.AppController {
	return controller.AppController{
		Cliente: r.NewClienteControl(),
	}
}
