package main

import (
	"fmt"
	"log"

	"github.com/labstack/echo"
	"github.com/rcjuarez/examen_go_rest/infrastructure/router"
	"github.com/rcjuarez/examen_go_rest/registry"
)

func main() {

	e := echo.New()

	r := registry.NewRegistry()
	e = router.NewRouter(e, r.NewAppController())

	fmt.Println("Servidor corriendo en el puerto localhost:8082")
	if err := e.Start(":8082"); err != nil {
		log.Fatalln(err)
	}
}

/*
Uso de los endpoints

curl --header "Content-Type: application/json" --request POST --data '{"Nombre": "Juan","Apellidos": "Perez Hernandez","Nombre_Usuario": "Ju","Correo_Electronico":"jperez@hotmail.com", "Contrasenia": "juanitoperez"}' localhost:8082/NutriNET/cliente

curl localhost:8082/NutriNET/cliente

curl localhost:8082/NutriNET/cliente/1

curl --header "Content-Type: application/json" --request PUT --data '{"Edad" : 39, "Estatura" : 1.80, "Peso" : 60, "GEB" : 1500}' localhost:8082/NutriNET/cliente/1
*/
